﻿using System.Collections.Generic;
using System.Linq;

namespace RomanNumeralCodeKata
{
    public static class RomanToArabic
    {
        public const string OVER_MAX_VALUE = "Application only supports arabic numbers up to 3999.";
        /*
            Conversion Chart
            I = 1
            V = 5
            X = 10
            L = 50
            C = 100
            D = 500
            M = 1000
        */
        /// <summary>
        /// Takes the roman numeral and converts it to it's arabic counter-part.
        /// </summary>
        /// <param name="romanNumerals"></param>
        /// <returns></returns>
        public static string GenerateArabicNumbers(string romanNumerals)
        {
            int arabicNumber = 0;
            arabicNumber = GenerateListOfNumbers(romanNumerals);
            if (arabicNumber > 3999)
                return OVER_MAX_VALUE;
            else
            {
                if (arabicNumber == 0)
                    return "Roman Numeral was invalid. Could not convert to Arabic Number.\n";
                return arabicNumber.ToString();
            }
        }
        /// <summary>
        /// Takes each character in the roman numeral string and converts it to it's literal
        /// arabic representation.
        /// </summary>
        /// <param name="romanNumerals"></param>
        /// <param name="numbers"></param>
        private static int GenerateListOfNumbers(string romanNumerals)
        {
            List<KeyValuePair<string, int>> valuePairs = new List<KeyValuePair<string, int>>();
            List<int> values = new List<int>();
            for (int i = 0; i < romanNumerals.Length; i++)
            {
                TranslateResponse response = Translate.FindArabicValue(romanNumerals[i].ToString());
                if (response.Success == true)
                    valuePairs.Add(response.RomanArabicValues);
            }
            for (int i = 0; i < valuePairs.Count; i++)
            {
                if ((i + 1) < valuePairs.Count)
                {
                    if (valuePairs[i].Value < valuePairs[i + 1].Value)
                    {
                        values.Add(valuePairs[i + 1].Value - valuePairs[i].Value);
                        i++;
                    }
                    else
                    {
                        values.Add(valuePairs[i].Value);
                    }
                }
                else
                {
                    values.Add(valuePairs[i].Value);
                }
            }
            return values.Sum();
        }
    }
}
