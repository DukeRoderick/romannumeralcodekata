﻿using System.Linq;

namespace RomanNumeralCodeKata
{
    public static class InputReader
    {
        public static InputConversionResponse ReadInputFromWindow(string input)
        {
            if (input.Any(char.IsDigit) == true)
            {
                int rawInputValue = ConvertInputToInteger(input);
                if (rawInputValue == -1)
                    return new InputConversionResponse(false, "Invalid Arabic Number Was Input");
                else
                    return new InputConversionResponse(true, ArabicToRoman.GenerateRomanNumeral(rawInputValue));
            }
            else
            {
                return new InputConversionResponse(true, RomanToArabic.GenerateArabicNumbers(input.ToUpper()));
            }
        }

        private static int ConvertInputToInteger(string stringInteger)
        {
            int result = -1;
            bool parse = int.TryParse(stringInteger, out result);
            if (parse == false)
                result = -1;
            return result;
        }
    }
}
