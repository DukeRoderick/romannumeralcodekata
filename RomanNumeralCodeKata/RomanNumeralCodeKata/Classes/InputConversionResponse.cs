﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumeralCodeKata
{
    public class InputConversionResponse
    {
        private bool success = false;
        private string response = string.Empty;

        public bool Success { get { return success; } }
        public string Response { get { return response; } }

        public InputConversionResponse(bool _success, string _response)
        {
            success = _success;
            response = _response;
        }
    }
}
