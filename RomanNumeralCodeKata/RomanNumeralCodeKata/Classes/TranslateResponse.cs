﻿using System.Collections.Generic;

namespace RomanNumeralCodeKata
{
    public class TranslateResponse
    {
        private KeyValuePair<string, int> romanArabicValues;
        private bool success = false;
        public KeyValuePair<string, int> RomanArabicValues { get { return romanArabicValues; } }
        public bool Success { get { return success; } }

        public TranslateResponse(bool _success, KeyValuePair<string, int> _response)
        {
            success = _success;
            romanArabicValues = _response;
        }
    }
}
