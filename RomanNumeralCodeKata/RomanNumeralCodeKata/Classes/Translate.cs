﻿using System.Collections.Generic;
using System.Linq;

namespace RomanNumeralCodeKata
{
    /// <summary>
    /// This class holds the conversion methods that return the representation of the input in it's other format.
    /// </summary>
    public static class Translate
    {
        public const string ROMAN_ONE = "I";
        public const string ROMAN_TWO = "II";
        public const string ROMAN_THREE = "III";
        public const string ROMAN_FOUR = "IV";
        public const string ROMAN_FIVE = "V";
        public const string ROMAN_SIX = "VI";
        public const string ROMAN_SEVEN = "VII";
        public const string ROMAN_EIGHT = "VIII";
        public const string ROMAN_NINE = "IX";
        public const string ROMAN_TEN = "X";
        public const string ROMAN_FOURTY = "XL";
        public const string ROMAN_FIFTY = "L";
        public const string ROMAN_NINETY = "XC";
        public const string ROMAN_HUNDRED = "C";
        public const string ROMAN_FOUR_HUNDRED = "CD";
        public const string ROMAN_FIVE_HUNDRED = "D";
        public const string ROMAN_NINE_HUNDRED = "CM";
        public const string ROMAN_THOUSAND = "M";
        /*
            Conversion Chart
            I = 1
            V = 5
            X = 10
            L = 50
            C = 100
            D = 500
            M = 1000
        */
        private static Dictionary<string, int> romansAndArabics = new Dictionary<string, int>();
        static Translate()
        {
            romansAndArabics.Add(ROMAN_ONE, 1);
            romansAndArabics.Add(ROMAN_TWO, 2);
            romansAndArabics.Add(ROMAN_THREE, 3);
            romansAndArabics.Add(ROMAN_FOUR, 4);
            romansAndArabics.Add(ROMAN_FIVE, 5);
            romansAndArabics.Add(ROMAN_SIX, 6);
            romansAndArabics.Add(ROMAN_SEVEN, 7);
            romansAndArabics.Add(ROMAN_EIGHT, 8);
            romansAndArabics.Add(ROMAN_NINE, 9);
            romansAndArabics.Add(ROMAN_TEN, 10);
            romansAndArabics.Add(ROMAN_FOURTY, 40);
            romansAndArabics.Add(ROMAN_FIFTY, 50);
            romansAndArabics.Add(ROMAN_NINETY, 90);
            romansAndArabics.Add(ROMAN_HUNDRED, 100);
            romansAndArabics.Add(ROMAN_FOUR_HUNDRED, 400);
            romansAndArabics.Add(ROMAN_FIVE_HUNDRED, 500);
            romansAndArabics.Add(ROMAN_NINE_HUNDRED, 900);
            romansAndArabics.Add(ROMAN_THOUSAND, 1000);
        }

        /// <summary>
        /// Reads a Roman Numeral and returns a Response that will hold a KeyValuePair holding the Roman Numeral
        /// and the Arabic Number representation if it converted successfully.
        /// </summary>
        /// <param name="romanNumeral"></param>
        /// <returns></returns>
        public static TranslateResponse FindArabicValue(string romanNumeral)
        {
            for(int i = 0; i < romansAndArabics.Count; i++)
            {
                var kvp = romansAndArabics.ElementAt(i);
                if (romanNumeral == kvp.Key)
                    return new TranslateResponse(true, kvp);
            }
            return new TranslateResponse(false, new KeyValuePair<string, int>("Error", -1));
        }

        /// <summary>
        /// Reads an Arabic Number and returns a Response that will hold a KeyValuePair holding the Roman Numeral
        /// and the Arabic Number represntation if it converted successfully.
        /// </summary>
        /// <param name="arabicNumber"></param>
        /// <returns></returns>
        public static TranslateResponse FindLargestRomanNumeralThatFits(int arabicNumber)
        {
            for(int i = romansAndArabics.Count - 1; i > -1; i--)
            {
                var kvp = romansAndArabics.ElementAt(i);
                if (arabicNumber - kvp.Value >= 0)
                    return new TranslateResponse(true, kvp);
            }
            return new TranslateResponse(false, new KeyValuePair<string, int>("Error", -1));
        }
    }
}
