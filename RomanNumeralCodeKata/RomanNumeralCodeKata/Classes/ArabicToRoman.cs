﻿using System.Text;

namespace RomanNumeralCodeKata
{
    public static class ArabicToRoman
    {
        public const string OVER_MAX_VALUE = "Application only supports roman numerals up to 3999.";

        /// <summary>
        /// Takes in any arabic number under 4000 and will properly convert it to roman numerals
        /// </summary>
        /// <param name="arabicValue">The number the user desires to conver to a roman numeral</param>
        /// <returns></returns>
        public static string GenerateRomanNumeral(int arabicValue)
        {
            //Wanted to handle 0.
            //Looked online and discovered on Wikipedia that nulla was used in
            //medieval times to represent 0
            if (arabicValue == 0)
                return "nulla";
            if (arabicValue > 3999)
                return OVER_MAX_VALUE;
            StringBuilder sbRomanNumerals = new StringBuilder();
            //Enter a repeating loop that will move through the will start reducing the arabicValue and adding the
            //relevant Roman Numerals
            while (true)
            {
                TranslateResponse response = Translate.FindLargestRomanNumeralThatFits(arabicValue);
                if (response.Success == true)
                {
                    sbRomanNumerals.Append(response.RomanArabicValues.Key);
                    arabicValue -= response.RomanArabicValues.Value;
                }
                else
                    break;
            }
            return sbRomanNumerals.ToString();
        }
    }
}
