﻿using System;

namespace RomanNumeralCodeKata
{
    #region Notes
    /*
        Roman Numeral Rules

        Repeating a number up to three times is addition
        Only I, X, C, and M can be repeated
        Numerals decreasing from left to right represent addition
        Small numeral on the left of a big number is subtraction
        A bar placed on top of a letter or string of letters increases the numeral by 1000

        Do not substract a number from one that is more than 10 times greater

        Only substract one number

        Only subtract powers of 10
    */
    #endregion

    class Program
    {
        private static string enterValue = "\nPlease enter a new value to be converted: ";
        private static string error = "\nPlease make sure you enter either a valid Arabic Number or Roman Numeral and try again.\n";
        static void Main(string[] args)
        {
            Intro();
        }

        private static void Intro()
        {
            Console.WriteLine("Welcome to the Roman Numeral <-> Arabic Number Converter!\n");
            Console.WriteLine("This application can handle any conversion up to 3999\n");
            AskForInput("Please enter either a roman numeral or an arabic number: ");
        }

        private static void AskForInput(string newPhrase)
        {
            Console.Write(newPhrase);
            InputConversionResponse response = InputReader.ReadInputFromWindow(Console.ReadLine());
            Console.WriteLine("\n" + response.Response);
            if (response.Success == true)
                AskForInput(enterValue);
            else
                AskForInput(error + enterValue);
        }
    }
}
