﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RomanNumeralCodeKata;

namespace RomanNumeralTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void One_Equals_I()
        {
            Assert.AreEqual("I", ArabicToRoman.GenerateRomanNumeral(1));
        }

        [TestMethod]
        public void Three_Equals_III()
        {
            Assert.AreEqual("III", ArabicToRoman.GenerateRomanNumeral(3));
        }

        [TestMethod]
        public void Four_DoesNotEqual_IIII()
        {
            Assert.AreNotEqual("IIII", ArabicToRoman.GenerateRomanNumeral(4));
        }

        [TestMethod]
        public void FiveHundred_Equals_D()
        {
            Assert.AreEqual("D", ArabicToRoman.GenerateRomanNumeral(500));
        }

        [TestMethod]
        public void OneThousandFiveHundred_Equals_MD()
        {
            Assert.AreEqual("MD", ArabicToRoman.GenerateRomanNumeral(1500));
        }

        [TestMethod]
        public void Thousand_Equals_M()
        {
            Assert.AreEqual("M", ArabicToRoman.GenerateRomanNumeral(1000));
        }
        [TestMethod]
        public void ThousandFiveHundredFifty_Equals_MDL()
        {
            Assert.AreEqual("MDL", ArabicToRoman.GenerateRomanNumeral(1550));
        }

        [TestMethod]
        public void OneThousandThreeHundredFourtyEight_Equals_MCCCXLVIII()
        {
            Assert.AreEqual("MCCCXLVIII", ArabicToRoman.GenerateRomanNumeral(1348));
        }

        [TestMethod]
        public void OneThousandThreeHundredFiftySeven_Equals_MCCCLVII()
        {
            Assert.AreEqual("MCCCLVII", ArabicToRoman.GenerateRomanNumeral(1357));
        }

        [TestMethod]
        public void ThreeThousandNineHundredNinetyNine_Equals_MMMCMXCIX()
        {
            Assert.AreEqual("MMMCMXCIX", ArabicToRoman.GenerateRomanNumeral(3999));
        }

        [TestMethod]
        public void FourThousand_Equals_MMMM()
        {
            Assert.AreEqual(ArabicToRoman.OVER_MAX_VALUE, ArabicToRoman.GenerateRomanNumeral(4000));
        }

        [TestMethod]
        public void I_Equals_One()
        {
            Assert.AreEqual("1", RomanToArabic.GenerateArabicNumbers("I"));
        }

        [TestMethod]
        public void XLIX_Equals_FourtyNine()
        {
            Assert.AreEqual("49", RomanToArabic.GenerateArabicNumbers("XLIX"));
        }

        [TestMethod]
        public void MCCCLVII_Equals_OneThousandThreeHundredFiftySeven()
        {
            Assert.AreEqual("1357", RomanToArabic.GenerateArabicNumbers("MCCCLVII"));
        }
        
        [TestMethod]
        public void MCCCXLVIII_Equals_OneThousandThreeHundredFourtyEight()
        {           
            Assert.AreEqual("1348", RomanToArabic.GenerateArabicNumbers("MCCCXLVIII"));
        }

        [TestMethod]
        public void InputTestOne()
        {
            InputConversionResponse testObj = new InputConversionResponse(true, "I");
            InputConversionResponse actualObj = InputReader.ReadInputFromWindow("1");
            Assert.AreEqual(testObj.Response, actualObj.Response);
        }

        [TestMethod]
        public void InputTestTwo()
        {
            InputConversionResponse testObj = new InputConversionResponse(true, "1");
            InputConversionResponse actualObj = InputReader.ReadInputFromWindow("I");
            Assert.AreEqual(testObj.Response, actualObj.Response);
        }

        [TestMethod]
        public void GetRomanCharFromDictionary()
        {
            Assert.AreEqual(Translate.ROMAN_FIVE, Translate.FindLargestRomanNumeralThatFits(5).RomanArabicValues.Key);
        }

        [TestMethod]
        public void GetArabicDigitFromDictionary()
        {
            Assert.AreEqual(50, Translate.FindArabicValue(Translate.ROMAN_FIFTY).RomanArabicValues.Value);
        }
    }
}
